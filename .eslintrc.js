module.exports = {
  extends: ["plugin:react/recommended", "prettier", "prettier/react"],
  plugins: ["react", "prettier"],
  rules: {
    "prettier/prettier": "error",
    "max-len": "off"
  },
  parser: "babel-eslint",
  "overrides": [
    {
      "files": [ "**/__mocks__/*.js", "**/__tests__/*.js", "*.test.js", "setupTests.js" ],
      "env": {
        "jest": true
      }
    }
  ],
  "settings": {
    "react": {
      "version": "detect"
    }
  }
};
