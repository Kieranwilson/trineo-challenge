# Instructions

- Spend 2 hours max on the exercise.
- Work with a complete and 'production ready' mindset, including making commits as you would for a PR that will be reviewed.
- Choose *two* of the four features below and implement them:
  1. Add a select list for films.  When a film is selected show details of the people in the film
  1. On the people component for each of the people found make an asynchronous request for the persons associated ships and render the responses
  1. Add pagination UI
  1. Add autocompleting search for people
- Feel free to show some flair if you feel like it and have the time to do so.
- Return a zip with your version controlled project via email.

## Prerequisites

- yarn: `npm install -g yarn`

## Setup

- `yarn install`

## Testing

- Single run: `yarn test`

## Development

- `yarn start`

# My Notes

Following the above I'm not investing a huge time into this. A few things I won't
be doing but would to productionize:
- Setting up module resolutions to /app so it's not all relative imports
- Doing more then basic styling
- Create unit tests around FetchCache - it can be tested but would take too long
- Error states for API calls
- More layout components and destructure that a bit more
- Actual unit tests, I got to this point after two hours so stopping now
