import React from 'react';
import style from './style.scss';

export const Loading = () => <div className={style.loading} />;

export default Loading;
