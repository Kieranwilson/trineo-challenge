import React from 'react';
import * as PropTypes from 'prop-types';
import style from './styles.scss';
import Loading from '../../Loading';
import Person from '../person';

export default class People extends React.PureComponent {
  static propTypes = {
    people: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string
      })
    )
  };

  render() {
    const { people } = this.props;
    return (
      <div className={style.header}>
        <h1>Star Wars People</h1>
        {!people && <Loading />}
        {people && (
          <div className={style.people}>
            {people.map(person => (
              <Person key={person.name} {...person} />
            ))}
          </div>
        )}
      </div>
    );
  }
}
