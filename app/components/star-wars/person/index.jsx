import React from 'react';
import * as PropTypes from 'prop-types';
import style from './style.scss';
import Ships from '../ships';

export default class Person extends React.PureComponent {
  static propTypes = {
    name: PropTypes.string,
    birth_year: PropTypes.string,
    gender: PropTypes.string,
    height: PropTypes.string,
    mass: PropTypes.string,
    starships: PropTypes.array
  };

  render() {
    const { name, birth_year, gender, height, mass, starships } = this.props;
    return (
      <div className={style.person}>
        <div className={style.name}>{name}</div>
        <div className={style.details}>
          <div className={style.detail}>
            <div className={style.title}>Born</div>
            <div className={style.value}>{birth_year}</div>
          </div>
          <div className={style.detail}>
            <div className={style.title}>Gender</div>
            <div className={style.value}>{gender}</div>
          </div>
          <div className={style.detail}>
            <div className={style.title}>Height</div>
            <div className={style.value}>{height}</div>
          </div>
          <div className={style.detail}>
            <div className={style.title}>Mass</div>
            <div className={style.value}>{mass}</div>
          </div>
        </div>
        <Ships ships={starships} />
      </div>
    );
  }
}
