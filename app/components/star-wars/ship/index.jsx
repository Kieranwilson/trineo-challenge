import React from 'react';
import * as PropTypes from 'prop-types';
import style from './style.scss';

export default class Ship extends React.PureComponent {
  render() {
    const { pilots, films, created, edited, url, ...relevant } = this.props;
    return (
      <div className={style.details}>
        {Object.keys(relevant).map(title => (
          <div key={title} className={style.detail}>
            <div className={style.title}>{title}</div>
            <div className={style.value}>{relevant[title]}</div>
          </div>
        ))}
      </div>
    );
  }
}
