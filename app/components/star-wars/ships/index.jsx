import React from 'react';
import * as PropTypes from 'prop-types';
import style from './style.scss';
import { fetchUrl } from '../../../services/StarWarsData';
import { updateNth } from '../../../utilities';
import Loading from '../../Loading';
import Ship from '../ship';

export default class Ships extends React.PureComponent {
  static propTypes = {
    ships: PropTypes.arrayOf(PropTypes.string)
  };

  state = {
    shipData: []
  };

  componentDidMount() {
    const { ships } = this.props;
    this.setState({ shipData: Array(ships.length) });
    const promises = ships.map((ship, i) => {
      return fetchUrl(ship).then(res =>
        this.setState(state => ({
          shipData: updateNth(state.shipData, res, i)
        }))
      );
    });
    Promise.all(promises)
      .then(() => {})
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { shipData } = this.state;
    return (
      <div className={style.ships}>
        <span className={style.header}>Ships</span>
        {shipData.map((ship, i) => (
          <div key={i}>{ship ? <Ship {...ship} /> : <Loading />}</div>
        ))}
      </div>
    );
  }
}
