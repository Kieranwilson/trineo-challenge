import React, { PureComponent } from 'react';
import People from '../components/star-wars/people';
import { fetchPeople, fetchUrl } from '../services/StarWarsData';
import { debounce } from '../utilities';

// If they are less then 100px from the bottom of the page
const bottomPageLoadMore = 100;

export default class App extends PureComponent {
  state = {
    loading: false,
    error: null,
    people: []
  };

  loadMore = () => {
    if (this.state.loading) return;
    this.setState({ loading: true });
    fetchUrl(this.state.people.next)
      .then(data => {
        this.setState(state => ({
          people: { next: data.next, results: [...state.people.results, ...data.results] },
          loading: false
        }));
      })
      .catch(error => this.setState({ error }));
  };

  shouldLoadMore = () =>
    this.state.people.next &&
    window.innerHeight + window.scrollY + bottomPageLoadMore >= document.body.offsetHeight;

  componentDidMount() {
    // Because I am wrapping it with another function it has to be defined here
    this.listener = () => debounce(this.shouldLoadMore() && this.loadMore(), 100);
    fetchPeople()
      .then(people => {
        this.setState({ people });
        window.addEventListener('scroll', this.listener);
      })
      .catch(error => this.setState({ error }));
  }

  componentWillUnmount() {
    if (this.listener) window.removeEventListener('scroll', this.listener);
  }

  render() {
    const { people } = this.state;
    return <People people={people.results} />;
  }
}
