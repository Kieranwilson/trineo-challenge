import fetchCache from '../fetchCache';
const baseUrl = 'http://swapi.co/api/';

const prependCors = () =>
  process.env.NODE_ENV === 'development' ? 'https://cors-anywhere.herokuapp.com/' : '';

const buildUrl = url => `${prependCors()}${url}`;

export const fetchPeople = () => fetchCache(buildUrl(`${baseUrl}people/`));
export const fetchUrl = url => fetchCache(buildUrl(url));
