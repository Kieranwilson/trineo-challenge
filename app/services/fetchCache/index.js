import EventEmitter from 'events';

const myEmitter = new EventEmitter();

// This is basic but because it is used in a few places, it's good to have it
// kept here
const resolveData = (data, success) => ({
  success,
  data
});

const resolution = (data, resolve, reject) => {
  if (data.success) {
    return resolve(data.data);
  }
  return reject(data.data);
};

const fetchAction = url =>
  new Promise((resolve, reject) => {
    fetch(url).then(response => {
      if (response.ok) {
        return resolve(response.json());
      }
      return reject(response);
    });
  });

const memoryCache = {};

export default url =>
  new Promise((resolve, reject) => {
    if (memoryCache[url] && memoryCache[url].data !== undefined) {
      // We have done this call already
      return resolution(memoryCache[url], resolve, reject);
    } else if (memoryCache[url] && memoryCache[url].pending) {
      // Wait until the call resolves then resolve with the same result
      myEmitter.once(`request_${url}_resolved`, data => resolution(data, resolve, reject));
    } else {
      memoryCache[url] = {
        pending: true
      };
      fetchAction(url)
        .then(data => {
          memoryCache[url] = resolveData(data, true);
          myEmitter.emit(`request_${url}_resolved`, memoryCache[url].data);
          return resolution(memoryCache[url], resolve, reject);
        })
        .catch(error => {
          memoryCache[url] = resolveData(error, false);
          myEmitter.emit(`request_${url}_resolved`, memoryCache[url].data);
          return resolution(memoryCache[url], resolve, reject);
        });
    }
  });
