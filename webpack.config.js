const path = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './app/index.html',
  filename: 'index.html',
  inject: 'body'
});

// use 'development' unless process.env.NODE_ENV is defined
const EnvPluginConfig = new webpack.EnvironmentPlugin({ NODE_ENV: 'development' });

module.exports = {
  entry: './app/index.js',
  output: {
    path: path.resolve('dist'),
    filename: 'index_bundle.js'
  },
  module: {
    rules: [
      { test: /\.jsx?$/, use: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.scss$/i,
        use: ['style-loader', { loader: 'css-loader', options: { modules: true } }, 'sass-loader']
      }
    ]
  },
  plugins: [HtmlWebpackPluginConfig, EnvPluginConfig],
  resolve: {
    extensions: ['.js', '.jsx']
  }
};
